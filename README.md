# Japanisch Lernen - 日本語を勉強しましょう

## Introduction

> Japanisch lernen mit Yuki! Verschiedene nützliche Vokabeln mit Aussprache und einem Story Modus um das gelernte Wissen zu überprüfen!

## Detailed Function

>Für Android Smartphone
>Nützliche Vokabeln im Alltag leicht und schnell gelernt.
>Alle Vokabeln mit Aussprache
>Im Story Modus kann mit Yuki zusammen gelernt werden.
>Geschrieben in Java

## How to use

> Install the apk on your smartphone

## Screenshot

| <a href="https://gitlab.com/haidepzai/japanisch-lernen" target="_blank">**Japanisch Lernen - 日本語を勉強しましょう**</a>


| [![Hai](https://i.ibb.co/2kp43FH/vokabeln.png)](https://gitlab.com/haidepzai/japanisch-lernen)

| [![Hai](https://i.ibb.co/Z6xYFY7/app.png)](https://gitlab.com/haidepzai/japanisch-lernen)


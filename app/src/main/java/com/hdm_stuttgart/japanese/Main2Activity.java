package com.hdm_stuttgart.japanese;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.hdm_stuttgart.japanese.helper.MusicPlayer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Main2Activity extends AppCompatActivity {

    MusicPlayer playboy = new MusicPlayer();

    private int[] songs = {
            R.raw.hallo,
            R.raw.danke,
            R.raw.entschuldigung,
            R.raw.bitte,
            R.raw.prost,
            R.raw.ciao, //5
            R.raw.wie_teuer,
            R.raw.badezimmer,
            R.raw.rechnung,
            R.raw.nihon,
            R.raw.nihongo, //10
            R.raw.hai,
            R.raw.iie,
            R.raw.beautiful,
            R.raw.lecker, //14
            R.raw.suki,
            R.raw.nani,
            R.raw.itsu,
            R.raw.sukoshi,
            R.raw.yokoso,
            R.raw.ohayo,
            R.raw.oyasumi,
            R.raw.ja_mata,
            R.raw.aishiteru,
            R.raw.domo_arigato,
            R.raw.dou_itashimashite,
            R.raw.gomennasai,
            R.raw.kein_problem,
            R.raw.omedetou,
            R.raw.sehr_gut,
            R.raw.shashin_totte,
            R.raw.wo_kommst_du_her,
    };

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        listView = findViewById(R.id.vocList);

        LinkedHashMap<String, String> japDe = new LinkedHashMap<>();

        japDe.put("Hallo", "こんにちは");
        japDe.put("Danke", "ありがとう");
        japDe.put("Entschuldigung", "すみません");
        japDe.put("Bitte sehr", "どうぞ");
        japDe.put("Prost", "かんぱい");
        japDe.put("Auf Wiedersehen", "さようなら"); //5
        japDe.put("Wie viel kostet es?", "いくらですか？");
        japDe.put("Wo ist die Toilette?", "お手洗いはどこですか?\nおてあらいはどこですか?");
        japDe.put("Die Rechnung bitte!", "お勘定お願いします\nおかんじょうおねがいします");
        japDe.put("Japan", "日本\nにほん");
        japDe.put("Japanisch", "日本語\nにほんご"); //10
        japDe.put("Ja", "はい");
        japDe.put("Nein", "いいえ");
        japDe.put("Wunderschön", "美しい\nうつくしい");
        japDe.put("Lecker", "美味しい\nおいしい");
        japDe.put("Ich mag dich", "好きです\nすきです");
        japDe.put("Was?", "何？\nなに？");
        japDe.put("Wann?", "いつ？");
        japDe.put("Etwas / Ein bisschen", "少し\nすこし");
        japDe.put("Willkommen!", "ようこそ");
        japDe.put("Guten Morgen!", "おはよう");
        japDe.put("Gute Nacht!", "おやすみなさい");
        japDe.put("Bis später!", "じゃまたね！");
        japDe.put("Ich liebe dich", "愛してる\nあいしてる");
        japDe.put("Vielen Dank", "どもありがとうございます");
        japDe.put("Bitte schön", "どういたしまして");
        japDe.put("Es tut mir Leid", "ごめんなさい");
        japDe.put("Kein Problem", "問題ありません\nもんだいありません");
        japDe.put("Herzlichen Glückwunsch", "おめでとうございます");
        japDe.put("Sehr gut!", "とても良い\nとてもよい");
        japDe.put("Darf ich ein Foto machen?", "写真をとてもいいですか？\nしゃしんをとてもいいですか？");
        japDe.put("Wo kommst du her?", "どこから来ましたか？\nどこからきましたか？");

        List<LinkedHashMap<String, String>> listItems = new ArrayList<>();
        SimpleAdapter adapter = new SimpleAdapter(this, listItems, R.layout.list_item,
                new String[]{"First Line", "Second Line"}, //Keys
                new int[]{R.id.text1, R.id.text2});
        //Durch HashMap iterieren
        Iterator it = japDe.entrySet().iterator();
        //Durch die Liste/HashMap japDe iterieren
        while(it.hasNext()){
            LinkedHashMap<String, String> resultMap = new LinkedHashMap<>();
            Map.Entry pair = (Map.Entry)it.next(); //nächstes Element von der Liste/HashMap (japDe)
            resultMap.put("First Line", pair.getKey().toString()); //Key
            resultMap.put("Second Line", pair.getValue().toString()); //Value
            listItems.add(resultMap);
        }

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playboy.play(Main2Activity.this, songs[position]);
            }
        });
    }
}

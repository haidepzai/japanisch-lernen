package com.hdm_stuttgart.japanese;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextSwitcher;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import androidx.appcompat.app.AppCompatActivity;

import com.hdm_stuttgart.japanese.helper.MusicPlayer;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView volumeView;
    SeekBar volumeControl;

    MusicPlayer player = new MusicPlayer();

    private TextSwitcher textSwitcher; //Like TextView but with Fade in - Fade out for TextView
    private TextView textView;
    private ImageButton nextButton;

    private int stringIndex = 0;
    private String[] dialog = {
            "Yuki: Hallo! Ich bin Yuki, deine Lehrerin!",
            "Yuki: Ich hoffe du hast gut gelernt.",
            "Yuki: Lass uns mit den Fragen beginnen!",
            "Yuki: こんにちは！ Das bedeutet?",
            "Yuki: Okay nächste Frage!",
            "Yuki: ありがとう！ Was heißt das?",
            "Yuki: Ich hoffe die Fragen sind nicht zu einfach...",
            "Yuki: Weiter geht's!",
            "Yuki: Was bedeutet 美味しい？", //Case 8
            "Yuki: Macht es Spaß mit mir zu lernen?",
            "Yuki: Ich hoffe ich bin eine gute Lehrerin! ♥",
            "Yuki: Und nun.. was heißt じゃまたね？",
            "Yuki: Ich hoffe du bist nicht müde!",
            "Yuki: Hier kommt die nächste.",
            "Yuki: Was heißt おやすみなさい？", //Case 14
            "Yuki: Alles Klar!", //Case 15
            "Yuki: Das war's erstmal für den Anfänger Kurs.",
            "Yuki: Ich hoffe es hat dir Spaß gemacht",
            "Yuki: Wir sehen uns in der nächsten Lektion"
    };


    private ImageView imageView;
    private ImageView answerView;
    private Button button1;
    private Button button2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        imageView = findViewById(R.id.imageView);
        answerView = findViewById(R.id.answerView);
        button1 = findViewById(R.id.button1);
        button2 = findViewById(R.id.button2);
        volumeView = findViewById(R.id.volumeView);

        textSwitcher = findViewById(R.id.textSwitcher);
        nextButton = findViewById(R.id.nextButton);
        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view){
                if (stringIndex == dialog.length-1){
                    player.stop();
                    Intent intent = new Intent(GameActivity.this, StartActivity.class);// New activity
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();
                } else {
                    textSwitcher.setText(dialog[++stringIndex]); //Else next Text from String
                }
                screenPopup();

            }
        });
        //Fade in - Fade out for text in the textView
        textSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                textView = new TextView(GameActivity.this); //Create new TextView with the settings below
                textView.setTextColor(Color.WHITE);
                textView.setTextSize(20);
                textView.setPadding(50,20,20,20);
                return textView;
            }
        });
        textSwitcher.setText(dialog[stringIndex]); //Start Text

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                answerView.setImageResource(R.drawable.correct);
                answerView.setVisibility(View.VISIBLE);
                volumeView.setVisibility(View.INVISIBLE);
                textSwitcher.setText("Korrekt!");
                imageView.setImageResource(R.drawable.happy);
            }
        });

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                button1.setVisibility(View.INVISIBLE);
                button2.setVisibility(View.INVISIBLE);
                nextButton.setVisibility(View.VISIBLE);
                volumeView.setVisibility(View.INVISIBLE);
                answerView.setImageResource(R.drawable.wrong);
                answerView.setVisibility(View.VISIBLE);
                textSwitcher.setText("Das ist leider falsch!");
                imageView.setImageResource(R.drawable.unhappy);
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (volumeControl.getVisibility() == View.INVISIBLE){
            volumeControl.setVisibility(View.VISIBLE);
        } else {
            volumeControl.setVisibility(View.INVISIBLE);
        }
    }

    private void screenPopup(){
        switch(stringIndex){
            case 3:
                button1.setText("Hallo / Guten Tag");
                button2.setText("Wie geht's dir?");
                button1.setVisibility(View.VISIBLE);
                button2.setVisibility(View.VISIBLE);
                nextButton.setVisibility(View.INVISIBLE);
                volumeView.setVisibility(View.VISIBLE);
                player.play(this,R.raw.hallo);
                break;
            case 4:
            case 9:
            case 12:
            case 15:
                imageView.setImageResource(R.drawable.smile);
                answerView.setVisibility(View.INVISIBLE);
                break;
            case 5:
                button1.setText("Danke!");
                button2.setText("Bitte");
                setViews();
                player.play(this,R.raw.danke);
                break;
            case 6:
                imageView.setImageResource(R.drawable.confident);
                answerView.setVisibility(View.INVISIBLE);
                break;
            case 8:
                button1.setText("Lecker");
                button2.setText("Schön");
                setViews();
                player.play(this,R.raw.lecker);
                break;
            case 10:
            case 16:
                imageView.setImageResource(R.drawable.smile2);
                answerView.setVisibility(View.INVISIBLE);
                break;
            case 11:
                button1.setText("Bis später!");
                button2.setText("Alles klar!");
                setViews();
                player.play(this,R.raw.ja_mata);
                break;
            case 14:
                button1.setText("Gute Nacht!");
                button2.setText("Guten Morgen!");
                setViews();
                player.play(this,R.raw.oyasumi);
                break;
        }
    }

    private void setViews(){
        button1.setVisibility(View.VISIBLE);
        button2.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
        volumeView.setVisibility(View.VISIBLE);
    }
}

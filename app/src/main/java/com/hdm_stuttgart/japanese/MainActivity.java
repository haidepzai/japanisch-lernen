package com.hdm_stuttgart.japanese;

import android.os.Bundle;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;


import androidx.appcompat.app.AppCompatActivity;

import com.hdm_stuttgart.japanese.helper.MusicPlayer;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    MusicPlayer playboy = new MusicPlayer();

    private int[] songs = {
            R.raw.hallo,
            R.raw.danke,
            R.raw.entschuldigung,
            R.raw.bitte,
            R.raw.prost,
            R.raw.ciao,
            R.raw.wie_teuer,
            R.raw.badezimmer,
            R.raw.rechnung
    };

    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        ArrayList<String> arrayList = new ArrayList<>();

        arrayList.add("Hallo / こんにちは");
        arrayList.add("Danke / ありがとう");
        arrayList.add("Entschuldigung / すみません");
        arrayList.add("Bitte / どうぞ");
        arrayList.add("Prost / かんぱい");
        arrayList.add("Auf Wiedersehen / さようなら");
        arrayList.add("Wie viel kostet es? / いくらですか？");
        arrayList.add("Wo ist die Toilette? / ");
        arrayList.add("Die Rechnung bitte! / お勘定お願いします");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, arrayList);

        listView.setAdapter(arrayAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                playboy.play(MainActivity.this, songs[position]);
            }
        });
    }

}

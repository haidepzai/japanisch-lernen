package com.hdm_stuttgart.japanese.helper;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import androidx.annotation.RawRes;

public class MusicPlayer {

    MediaPlayer player;

    public void play(Context c, @RawRes int sound) {

        player = MediaPlayer.create(c, sound);
        Log.i("Info", "Player created!");

        player.start();
    }

    public void loop(boolean looping) {
        player.setLooping(looping);
    }

    public void pause() {
        if (player != null) {
            Log.i("Info", "Player paused!");
            player.pause();
        }
    }

    public void stop() {
        Log.i("Info", "Request Player Stop!");
        stopPlayer();
    }

    private void stopPlayer() {
        if (player != null) {
            player.release();
            player = null;
            Log.i("Info", "Player stopped!");
        }
    }

    public void start() {
        if (player != null) {
            Log.i("Info", "Player started!");
            player.start();
        }
    }
}
